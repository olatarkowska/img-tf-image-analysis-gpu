#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2020 Tong LI <tongli.bioinfo@protonmail.com>
#
# Distributed under terms of the BSD-3 license.

"""
Parameters
----------
image : ndarray
    Input image data. Will be converted to float.
mode : str
    One of the following strings, selecting the type of noise to add:

    'gauss'     Gaussian-distributed additive noise.
    'poisson'   Poisson-distributed noise generated from the data.
    's&p'       Replaces random pixels with 0 or 1.
    'speckle'   Multiplicative noise using out = image + n*image,where
                n is uniform noise with specified mean & variance.
"""
import argparse
import numpy as np
import os
import cv2
import tifffile as tf
import time



def noisy(image, noise_typ):
    if noise_typ == "gauss":
        row,col,ch= image.shape
        mean = 0
        var = 0.1
        sigma = var**0.5
        gauss = np.random.normal(mean,sigma,(row,col,ch))
        gauss = gauss.reshape(row,col,ch)
        noisy = image + gauss
        return noisy
    elif noise_typ == "sp":
        row,col,ch = image.shape
        s_vs_p = 0.5
        amount = 0.004
        out = np.copy(image)
        # Salt mode
        num_salt = np.ceil(amount * image.size * s_vs_p)
        coords = [np.random.randint(0, i - 1, int(num_salt))
              for i in image.shape]
        out[coords] = 200

        # Pepper mode
        num_pepper = np.ceil(amount* image.size * (1. - s_vs_p))
        coords = [np.random.randint(0, i - 1, int(num_pepper))
              for i in image.shape]
        out[coords] = 0
        return out
    elif noise_typ == "poisson":
        vals = len(np.unique(image))
        vals = 2 ** np.ceil(np.log2(vals))
        noisy = np.random.poisson(image * vals) / float(vals)
        return noisy
    elif noise_typ =="speckle":
        row,col,ch = image.shape
        gauss = np.random.randn(row,col,ch)
        gauss = gauss.reshape(row,col,ch)
        noisy = image + image * gauss
        return noisy


def main(args):
    use_gpu=args.GPU
    img = np.ones([10000, 10000, 3], dtype=np.uint16) * 100
    kernel_size = 30
    sigma = 9
    noise = noisy(img, "sp")
    out_dir = '/test_gpu/'
    tf.imsave('%s/noise.tif' %out_dir, noise, bigtiff=True)
    if use_gpu:
        import tensorflow_addons as tfa

        start = time.time()
        blurred = np.array(tfa.image.gaussian_filter2d(noise, kernel_size, sigma))
        end = time.time()
        print(end - start)
        # print(type(blurred))
        tf.imsave('%s/blurred_noise_cpu.tif' %out_dir, blurred, bigtiff=True)

    else:

        print('do not use GPU')
        import os
        os.environ["CUDA_VISIBLE_DEVICES"] = "-1"
        import tensorflow_addons as tfa

        start = time.time()
        blurred = np.array(tfa.image.gaussian_filter2d(noise, kernel_size, sigma))
        end = time.time()
        print(end - start)
        tf.imsave('%s/blurred_noise_gpu.tif' %out_dir, blurred, bigtiff=True)



if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument('--GPU', dest='GPU', action='store_true')
    parser.add_argument('--CPU', dest='CPU', action='store_false')
    parser.set_defaults(feature=False)

    args = parser.parse_args()

    main(args)
