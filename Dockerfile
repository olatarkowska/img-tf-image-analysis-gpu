FROM tensorflow/tensorflow:2.3.0-gpu

RUN apt update -y && \
	apt install libgl1-mesa-glx -y

RUN pip install opencv-python tifffile tensorflow-addons
