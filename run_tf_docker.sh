#! /bin/sh
#
# run_tf_docker.sh
# Copyright (C) 2020 Tong LI <tongli.bioinfo@protonmail.com>
#
# Distributed under terms of the BSD-3 license.
#


docker run -it --runtime nvidia \
	-v /home/ubuntu/Documents/img-tf-image-analysis-gpu:/test-gpu \
	gitlab-registry.internal.sanger.ac.uk/tl10/img-tf-image-analysis-gpu:latest \
	#bash
	#python -c 'import tensorflow as tf; tf.test.is_gpu_available()'
